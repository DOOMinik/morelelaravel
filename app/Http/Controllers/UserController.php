<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Retrieve all users from DB
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        return response()->json(User::all());
    }
}
