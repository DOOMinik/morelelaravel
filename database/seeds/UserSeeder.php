<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder
{

    /**
     * List of users to populate in table
     *
     * @var array
     */
    private $users = [
        ['name' => 'Piotr','email' => 'jace@gtestg.pl','phone' => 758559456],
        ['name' => 'Agata','email' => 'aga@gtestg.pl','phone' =>555222111],
        ['name' => 'Marcelina','email' => 'amrc@gtestg.pl','phone' =>222333444],
        ['name' => 'Adam','email' => 'adam@gtewsth.pl','phone' =>555666777]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert($this->users);
    }
}
